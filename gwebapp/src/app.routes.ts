import { Component } from '@angular/core';
import { ArtistaComponent } from './app/components/artista/artista.component';
import { SearchComponent } from './app/components/search/search.component';
import { HomeComponent } from './app/components/home/home.component';
import {Routes} from '@angular/router';

export const ROUTES: Routes=[
    { path: 'home', component:HomeComponent},
    { path: 'search', component:SearchComponent},
    { path: 'artista', component:ArtistaComponent},
    { path: '', pathMatch:'full', redirectTo:'home'},
    { path: '**', pathMatch:'full', redirectTo:'home'},
]