import { DataService } from './../../service/data.service.ts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  pais:any;
  constructor(private dataservice:DataService) { }

  ngOnInit() {
    
    this.dataservice.getPais().subscribe(data=>{
      this.pais = data;
      console.log(data);
    })
  }

}
